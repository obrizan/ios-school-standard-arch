//
//  MainScenario.h
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/13/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginScenario.h"

@interface MainScenario : NSObject<LoginScenarioDelegate>

+(MainScenario *)mainScenarioWithWindow:(UIWindow *)w;

-(void)start;

@end
