//
//  SignUpVC.h
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/12/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignUpVCDelegate <NSObject>

-(void)didSignInTapped;

-(void)didSignUpTapped:(NSString *)login password:(NSString *)password;


@end

@interface SignUpVC : UIViewController

@property (nonatomic, weak, readonly) NSObject<SignUpVCDelegate> *delegate;

+(SignUpVC *)signUpVCWithDelegate:(NSObject<SignUpVCDelegate> *)delegate;

-(void)showError:(NSString *)message;

@end
