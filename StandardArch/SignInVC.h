//
//  SignInVC.h
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/12/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignInVCDelegate <NSObject>

-(void)didLoginTapped:(NSString *)login password:(NSString *)password;

-(void)didRegisterTapped;


@end

@interface SignInVC : UIViewController

@property (weak, nonatomic, readonly) NSObject<SignInVCDelegate> *delegate;


+(SignInVC *)signInVCWithDelegate:(NSObject<SignInVCDelegate> *)delegate;

-(void)showError:(NSString *)message;


@end
