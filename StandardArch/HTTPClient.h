//
//  HTTPClient.h
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/15/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPClient : NSObject

+(void)sendRequest:(NSURLRequest *)request completion:(void(^)(NSHTTPURLResponse *response, NSData *body))completion;

@end
