//
//  HTTPClient.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/15/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "HTTPClient.h"

@implementation HTTPClient

+(void)sendRequest:(NSURLRequest *)request completion:(void(^)(NSHTTPURLResponse *response, NSData *body))completion {
    
    if ([request.URL.path isEqualToString:@"/api/v1/login"]) {
        
        NSString *response = @"{\"error\": \"Incorrect password.\"}";
        NSData *body = [response dataUsingEncoding:NSUTF8StringEncoding];
        
        completion(nil, body);
        return;
        
    }
    
}

@end
