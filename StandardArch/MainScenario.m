//
//  MainScenario.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/13/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "MainScenario.h"

@interface MainScenario ()
{
    UIWindow *_window;
    LoginScenario *_loginScenario;
}

@end

@implementation MainScenario



+(MainScenario *)mainScenarioWithWindow:(UIWindow *)w {
    return [[MainScenario alloc] initWithWindow:w];
}

-(MainScenario *)initWithWindow:(UIWindow *)w {
    NSParameterAssert(w);
    
    self = [super init];
    if (self) {
        _window = w;
    }
    return self;
}

-(void)start {
    
    _loginScenario = [LoginScenario loginScenarioWithWindow:_window delegate:self];
    
    [_loginScenario start];
    
}

#pragma mark - LoginScenarioDelegate

-(void)didFinishScenario:(LoginScenario *)scenario {
    
    _loginScenario = nil;

    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"vc"];

    _window.rootViewController = vc;
    
}

@end
