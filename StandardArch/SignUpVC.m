//
//  SignUpVC.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/12/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "SignUpVC.h"

@interface SignUpVC ()
@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password1;
@property (weak, nonatomic) IBOutlet UITextField *password2;

@property (nonatomic, weak) NSObject<SignUpVCDelegate> *delegate;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation SignUpVC

+(SignUpVC *)signUpVCWithDelegate:(NSObject<SignUpVCDelegate> *)delegate {
    
    NSParameterAssert(delegate);
    
    SignUpVC *vc = [[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    
    vc.delegate = delegate;
    
    return vc;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)showError:(NSString *)message {
    self.errorLabel.text = message;
}

- (IBAction)loginTapped:(id)sender {
    
    [self.delegate didSignInTapped];
}

- (IBAction)registerTapped:(id)sender {
    
    // Проверить, валидна ли форма.
    
    [self.delegate didSignUpTapped:self.login.text password:self.password1.text];
}

@end
