//
//  Scenario.h
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/12/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "SignInVC.h"
#import "SignUpVC.h"

@class LoginScenario;


@protocol LoginScenarioDelegate <NSObject>

-(void)didFinishScenario:(LoginScenario *)scenario;

@end


@interface LoginScenario : NSObject<SignUpVCDelegate, SignInVCDelegate>

@property (nonatomic, readonly) NSObject<LoginScenarioDelegate> *delegate;
@property (nonatomic, readonly, weak) UIWindow *window;


+(LoginScenario *)loginScenarioWithWindow:(UIWindow *)window  delegate:(NSObject<LoginScenarioDelegate>*)delegate;

-(void)start;


@end
