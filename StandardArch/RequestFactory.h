//
//  RequestFactory.h
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/15/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestFactory : NSObject

+(NSURLRequest *)requestForLogin:(NSString *)login password:(NSString *)password;

+(NSError *)modelForLoginResponse:(NSURLResponse *)response body:(NSData *)body;

@end
