//
//  Scenario.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/12/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "LoginScenario.h"

#import "BackendService.h"


@interface LoginScenario ()
{
    SignInVC *_signInVC;
    SignUpVC *_signUpVC;
    NSObject<LoginScenarioDelegate> *_delegate;
}

@property (nonatomic, weak) UIViewController *vc;
@property (nonatomic, weak) UIWindow *window;

@end

@implementation LoginScenario

+(LoginScenario *)loginScenarioWithWindow:(UIWindow *)window  delegate:(NSObject<LoginScenarioDelegate>*)delegate; {
    
    return [[LoginScenario alloc] initWithWindow:window delegate:delegate];
    
}


-(LoginScenario *)initWithWindow:(UIWindow *)window delegate:(NSObject<LoginScenarioDelegate>*)delegate{
    
    NSParameterAssert(window);
    
    self = [super init];
    if (self) {
        self.window = window;
        _delegate = delegate;
    }
    return self;
}


// Сама логика сценария.

-(void)start {
    
    _signUpVC = [SignUpVC signUpVCWithDelegate:self];
    
    self.window.rootViewController = _signUpVC;
    
}

#pragma mark - SignUpVCDelegate

-(void)didSignInTapped {
    
    _signInVC = [SignInVC signInVCWithDelegate:self];        
    [_signUpVC presentViewController:_signInVC animated:YES completion:nil];
    
}


-(void)didSignUpTapped:(NSString *)login password:(NSString *)password {
    
    // Обращение к сервису регистрации пользователей.
    [BackendService registerUser:login password:password completion:^(NSError *e) {
        
        if (!e) {
            
            // Завершить сценарий.
            [self _finishScenario];
            
            
        } else {
            
            // Показать сообщение об ошибке.
            [_signUpVC showError:e.userInfo[@"msg"]];
            
        }
        
        
    }];
    
    
}

#pragma mark - SignInVCDelegate

-(void)didLoginTapped:(NSString *)login password:(NSString *)password {
    
    // Обращение к сервису логина пользователей.
    [BackendService loginUser:login password:password completion:^(NSError *e) {
        
        if (!e) {
            
            [self _finishScenario];
            
        } else {
            
            // Показать сообщение об ошибке.
            [_signInVC showError:e.userInfo[@"msg"]];
            
        }
        
    }];
    
}



-(void)didRegisterTapped {
    
    [_signInVC dismissViewControllerAnimated:YES completion:nil];
    
}



#pragma mark - Private methods.

-(void)_finishScenario {
    
    [self.delegate didFinishScenario:self];
    
}

@end
