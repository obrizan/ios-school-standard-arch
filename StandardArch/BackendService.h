//
//  BackendService.h
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/13/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackendService : NSObject

/// Signature: "registerUser:password:completion:"
+(void)registerUser:(NSString *)userName
           password:(NSString *)password
         completion:(void(^)(NSError *e))completion;

+(void)loginUser:(NSString *)userName password:(NSString *)password completion:(void(^)(NSError *e))completion;


@end
