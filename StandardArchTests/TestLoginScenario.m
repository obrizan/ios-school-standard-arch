//
//  TestLoginScenario.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/14/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <OCMock/OCMock.h>

#import "LoginScenario.h"
#import "SignUpVC.h"
#import "BackendService.h"

@interface TestLoginScenario : XCTestCase

@end

@implementation TestLoginScenario

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testCorrectInitialization {
    
    UIWindow *w = [UIWindow new];
    
    NSObject *o = [NSObject new];
    
    LoginScenario *scenario = [LoginScenario loginScenarioWithWindow:w delegate:o];
    
    XCTAssert(scenario.delegate == o);
    XCTAssert(scenario.window == w);
}


-(void)testStart {
    
    id signUpMock = OCMClassMock([SignUpVC class]);
    OCMStub([signUpMock signUpVCWithDelegate:[OCMArg any]]).andReturn(signUpMock);
    
    id windowMock = OCMClassMock([UIWindow class]);
    
    id protocolMock = OCMProtocolMock(@protocol(LoginScenarioDelegate));
    
    LoginScenario *scenario = [LoginScenario loginScenarioWithWindow:windowMock delegate:protocolMock];
    
    // Активация.
    [scenario start];
    
    // Проверяем, иницилазировался ли SingUPVC.
    OCMVerify([signUpMock signUpVCWithDelegate:scenario]);
    
    // Проверяем, что мы установили view controller as a root VC.
    OCMVerify([windowMock setRootViewController:[OCMArg isEqual:signUpMock]]);
    
}


-(void)testCorrectSignUp {
    
    id backendMock = OCMClassMock([BackendService class]);
    
    OCMStub([backendMock registerUser:[OCMArg any] password:[OCMArg any] completion:([OCMArg invokeBlock])]);
    
    UIWindow *w = [UIWindow new];
    id protocolMock = OCMProtocolMock(@protocol(LoginScenarioDelegate));
    
    // Активация.
    LoginScenario *scenario = [LoginScenario loginScenarioWithWindow:w delegate:protocolMock];
    [scenario didSignUpTapped:@"login" password:@"password"];
    
    // Проверки.
    OCMVerify([protocolMock didFinishScenario:scenario]);
    
}


-(void)testIncorrectSignUp {
    
    id backendMock = OCMClassMock([BackendService class]);
    NSError *e = [NSError errorWithDomain:NSURLErrorDomain code:0 userInfo:@{@"msg": @"test1P983G4BFOPIQ9W8B4O8TBQ1P3498TU"}];
    OCMStub([backendMock registerUser:[OCMArg any] password:[OCMArg any] completion:([OCMArg invokeBlockWithArgs:e, nil])]);
    
    id signUpMock = OCMClassMock([SignUpVC class]);
    OCMStub([signUpMock signUpVCWithDelegate:[OCMArg any]]).andReturn(signUpMock);
    
    UIWindow *w = [UIWindow new];
    id protocolMock = OCMProtocolMock(@protocol(LoginScenarioDelegate));
    
    // Активация.
    LoginScenario *scenario = [LoginScenario loginScenarioWithWindow:w delegate:protocolMock];
    [scenario start];
    [scenario didSignUpTapped:@"login" password:@"password"];
    
    // Проверка.
    OCMVerify([signUpMock showError:@"test1P983G4BFOPIQ9W8B4O8TBQ1P3498TU"]);
}

-(void)testCorrectSignIn {
    
    id backendMock = OCMClassMock([BackendService class]);
    
    OCMStub([backendMock loginUser:[OCMArg any] password:[OCMArg any] completion:([OCMArg invokeBlock])]);
    
    UIWindow *w = [UIWindow new];
    id protocolMock = OCMProtocolMock(@protocol(LoginScenarioDelegate));
    
    // Активация.
    LoginScenario *scenario = [LoginScenario loginScenarioWithWindow:w delegate:protocolMock];
    [scenario didLoginTapped:@"login" password:@"password"];
    
    // Проверки.
    OCMVerify([protocolMock didFinishScenario:scenario]);
    
}


-(void)testIncorrectSignIn {
    
    id backendMock = OCMClassMock([BackendService class]);
    NSError *e = [NSError errorWithDomain:NSURLErrorDomain code:0 userInfo:@{@"msg": @"test1P983G4BFOPIQ9W8B4O8TBQ1P3498TU"}];
    
    OCMStub([backendMock loginUser:[OCMArg any] password:[OCMArg any] completion:([OCMArg invokeBlockWithArgs:e, nil])]);
    
    id signInMock = OCMClassMock([SignInVC class]);
    OCMStub([signInMock signInVCWithDelegate:[OCMArg any]]).andReturn(signInMock);
    
    UIWindow *w = [UIWindow new];
    id protocolMock = OCMProtocolMock(@protocol(LoginScenarioDelegate));
    
    // Активация.
    LoginScenario *scenario = [LoginScenario loginScenarioWithWindow:w delegate:protocolMock];
    [scenario start];
    [scenario didSignInTapped];
    [scenario didLoginTapped:@"login" password:@"password"];
    
    // Проверка.
    OCMVerify([signInMock showError:@"test1P983G4BFOPIQ9W8B4O8TBQ1P3498TU"]);
}


-(void)testHideRegisterVC {
    
    id signInMock = OCMClassMock([SignInVC class]);
    OCMStub([signInMock signInVCWithDelegate:[OCMArg any]]).andReturn(signInMock);
    
    UIWindow *w = [UIWindow new];
    id protocolMock = OCMProtocolMock(@protocol(LoginScenarioDelegate));
    
    // Активация.
    LoginScenario *scenario = [LoginScenario loginScenarioWithWindow:w delegate:protocolMock];
    [scenario start];
    [scenario didSignInTapped];
    [scenario didRegisterTapped];
    
    OCMVerify([signInMock dismissViewControllerAnimated:YES completion:nil]);

}

@end
