//
//  BackendService.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/13/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "BackendService.h"

#import "HTTPClient.h"
#import "RequestFactory.h"


@implementation BackendService

+(void)registerUser:(NSString *)userName password:(NSString *)password completion:(void(^)(NSError *e))completion {
    
    NSError *e = [NSError errorWithDomain:NSCocoaErrorDomain code:1 userInfo:@{@"msg": @"Username already exists."}];
    
    completion(e);
    
}

+(void)loginUser:(NSString *)userName password:(NSString *)password completion:(void(^)(NSError *e))completion {
    
    NSURLRequest *request = [RequestFactory requestForLogin:userName password:password];
    
    [HTTPClient sendRequest:request completion:^(NSHTTPURLResponse *response, NSData *body) {
        
        NSError *e = [RequestFactory modelForLoginResponse:response body:body];
        
        completion(e);
        
    }];
}

@end
