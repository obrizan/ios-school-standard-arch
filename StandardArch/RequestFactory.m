//
//  RequestFactory.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/15/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "RequestFactory.h"

@implementation RequestFactory

+(NSURLRequest *)requestForLogin:(NSString *)login password:(NSString *)password {
    
    NSURL *url = [NSURL URLWithString:@"http://localhost/api/v1/login"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSDictionary *d = @{
                        @"login": login,
                        @"password": password
                        };
    
    NSError *e = nil;
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:d options:0 error:&e];
    
    return request;
    
}

+(NSError *)modelForLoginResponse:(NSURLResponse *)response body:(NSData *)body {
    
    NSError *e = nil;
    NSDictionary *d = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:body options:0 error:&e];
    
    NSString *errorMessage = d[@"error"];
    if (d[@"error"]) {
        return [NSError errorWithDomain:NSCocoaErrorDomain code:0 userInfo:@{@"msg": errorMessage}];
    } else {
        return nil;
    }
    
}

@end
