//
//  SignInVC.m
//  StandardArch
//
//  Created by Vladimir Obrizan on 12/12/16.
//  Copyright © 2016 Design and Test Lab. All rights reserved.
//

#import "SignInVC.h"

@interface SignInVC ()

@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) NSObject<SignInVCDelegate> *delegate;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation SignInVC

+(SignInVC *)signInVCWithDelegate:(NSObject<SignInVCDelegate> *)delegate {
    
    NSParameterAssert(delegate);
    
    SignInVC *vc = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
    vc.delegate = delegate;
    return vc;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)loginTapped:(id)sender {
    [self.delegate didLoginTapped:self.login.text password:self.password.text];
}

- (IBAction)registerTapped:(id)sender {
    [self.delegate didRegisterTapped];
}

-(void)showError:(NSString *)message {
    self.errorLabel.text = message;
}

@end
